import { style } from '@mui/system'
import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import MovieCard from '../components/MovieCard'
import styles from './reviews.module.css'

const Reviews: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Cool Movies App</title>
        <meta name="description" content="Cool Movies App" />
      </Head>

      <main className={styles.main}>

        <h1 className={styles.title}>
          Welcome to Cool Movies!
        </h1>
        <br />

        <div className={styles.MovieCardLayout}>
            <MovieCard className={styles.MovieCard} title="Title01" hRef="/" releaseDate="2020/02/02" imgUrl="https://via.placeholder.com/150x200" />
            <MovieCard className={styles.MovieCard} title="Title01" hRef="/" releaseDate="2020/02/02" imgUrl="https://via.placeholder.com/150x200" />
            <MovieCard className={styles.MovieCard} title="Title01" hRef="/" releaseDate="2020/02/02" imgUrl="https://via.placeholder.com/150x200" />
            <MovieCard className={styles.MovieCard} title="Title01" hRef="/" releaseDate="2020/02/02" imgUrl="https://via.placeholder.com/150x200" />
        </div>

      </main>

    </div>
  )
}

export default Reviews
