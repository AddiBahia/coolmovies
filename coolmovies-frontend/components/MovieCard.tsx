import Link from 'next/link';
import Image from 'next/image';

/*import React from 'react';*/

import styles from './MovieCard.module.css';


const MovieCard = (props:any) =>  {

    return (
        <Link href={props.hRef}>
            <a className={styles.cardBackground}>
                {/* <Image src={props.imgUrl} width={150} height={200} layout="responsive" /> */}
            <img src="https://via.placeholder.com/150x200" alt="" />

                <h1 className={styles.title}>{props.title}</h1>
                <h2 className={styles.releaseDate}>{props.releaseDate}</h2>

                <button className={styles.btnShowReviews}>show reviews</button>
                <br />
                <button className={styles.btnAddReview}>add new review</button>
            </a>
        </Link>
    );
}

export default MovieCard;